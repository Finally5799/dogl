# D OpenGL Project

Simple GLFW project with D, to get started, copy and paste glfw3.lib in the root and run dub to build.

## Build configurations

Currently I set up the project to build the app with static linking, because personally I like my app to be self contained.