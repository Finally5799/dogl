import bindbc.glfw;
import std.stdio;

void main()
{
	writeln("Loaded");
	if (!glfwInit())
	{
    	// Handle initialization failure
	}
	GLFWwindow* window = glfwCreateWindow(640, 480, "My Title", null, null);
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, &key_callback);

	while (!glfwWindowShouldClose(window))
	{
    	// Keep running
        glfwSwapBuffers(window);
        glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
}
extern(C) nothrow
{
	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
    	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        	glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}